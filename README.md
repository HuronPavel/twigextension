# README #
## Functions list ##
* leftpad(str,length,char=' ')
* rightpad(str,length,char=' ')
* bothpad(str,length,char=' ')

## Using ##

Add to services.yml
```
services:
    app.strpad_twig_extension:
        class: huron\TwigExtension\StrPadExtension
        public: false
        tags:
            - { name: twig.extension }
```
