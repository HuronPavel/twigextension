<?php

namespace huron\TwigExtension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class StrPadExtension extends AbstractExtension
{
    public function getFilters() {
        return [
            new TwigFilter('leftpad',[$this, 'LeftPad']),
            new TwigFilter('rightpad',[$this, 'RightPad']),
            new TwigFilter('bothpad',[$this, 'BothPad']),
        ];
    }
    
    public function LeftPad($source, $length, $symbol = ' ')
    {
        return str_pad($source, $length, $symbol, STR_PAD_LEFT);
    }
    
    public function RigthPad($source, $length, $symbol = ' ')
    {
        return str_pad($source, $length, $symbol, STR_PAD_RIGHT);
    }
    
    public function BothPad($source, $length, $symbol = ' ')
    {
        return str_pad($source, $length, $symbol, STR_PAD_BOTH);
    }
}
